<?php
/**
 * Created by PhpStorm.
 * User: gmaimone
 * Date: 11/3/16
 * Time: 11:57 AM
 */

namespace AppBundle\Forms;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ClubType
 *
 * @package AppBundle\Forms
 */
class ClubType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder builder
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'nombre', TextType::class, [
                'attr' => [
                    'placeholder' => 'Nombre',
                    'maxlength' => '50',
                    'class' => 'form-control'
                ],
                'label' => false]
            )
            ->add(
                'telefono', TextType::class, [
                    'attr' => [
                        'placeholder' => 'Telefono',
                        'maxlength' => '50',
                        'class' => 'form-control'
                    ],
                    'label' => false]
            )
            ->add(
                'jugadores',  EntityType::class, [
                    'class' => 'AppBundle:Jugadores',

                    'choice_label' => 'Nombre',
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'expanded' => false,
                    'multiple' => true,
                    'label' => false,
                    'by_reference' => false,
                    'placeholder' => 'Please Select..',
                ]
            )
            ->add(
                'borrado', ChoiceType::class, [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'choices' => ['Activo' => 1, 'Inactivo' => 0],
                    'expanded' => false,
                    'multiple' => false,
                    'label' => false,
                    'placeholder' => 'Seleccione..'
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver resolver
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(['data_class' => 'AppBundle\Model\Club']);
    }
}
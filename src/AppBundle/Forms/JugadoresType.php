<?php
/**
 * Created by PhpStorm.
 * User: gmaimone
 * Date: 11/3/16
 * Time: 11:57 AM
 */

namespace AppBundle\Forms;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class JugadoresType
 *
 * @package AppBundle\Forms
 */
class JugadoresType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder builder
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'nombre', TextType::class, [
                    'attr' => [
                        'placeholder' => 'Nombre',
                        'maxlength' => '50',
                        'class' => 'form-control'
                    ],
                    'label' => false]
            );
    }

    /**
     * @param OptionsResolver $resolver resolver
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(['data_class' => 'AppBundle\Model\Jugadores']);
    }
}
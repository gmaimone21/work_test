<?php
/**
 * Created by PhpStorm.
 * User: gustavo
 * Date: 21/05/17
 * Time: 10:03
 */

namespace AppBundle\Model;


class Jugadores
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var string
     */
    protected $nombre;

    /**
     * @var Club
     */
    protected $club;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime('now', new \DateTimeZone('UTC')));

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Nombre
     *
     * @param string $nombre
     *
     * @return $this
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get Nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set Club
     *
     * @param Club|null $club
     * @return $this
     */
    public function setClub(Club $club = null)
    {
        $this->club = $club;

        return $this;
    }

    /**
     * Get Club
     *
     * @return Club
     */
    public function getClub()
    {
        return $this->club;
    }




}
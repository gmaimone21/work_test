<?php
/**
 * Created by PhpStorm.
 * User: gustavo
 * Date: 21/05/17
 * Time: 10:02
 */

namespace AppBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;

class Club
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var string
     */
    protected $nombre;

    /**
     * @var string
     */
    protected $telefono;

    /**
     * @var bool
     */
    protected $borrado;

    /**
     * @var string
     */
    protected $jugadores;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime('now', new \DateTimeZone('UTC')));
        $this->jugadores = new ArrayCollection();

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Nombre
     *
     * @param string $nombre
     *
     * @return $this
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get Nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return boolean
     */
    public function getBorrado()
    {
        return $this->borrado;
    }

    /**
     * @param boolean $borrado
     */
    public function setBorrado($borrado)
    {
        $this->borrado = $borrado;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add Player to the Club
     *
     * @param Jugadores $jugadores Jugadores class
     *
     * @return void
     */
    public function setJugadores(Jugadores $jugadores)
    {
        $this->jugadores[] = $jugadores;
        $jugadores->setClub($this);
    }

    /**
     * Get all Jugadores from Club
     *
     * @return \AppBundle\Model\Jugadores
     */
    public function getJugadores()
    {
        return $this->jugadores;
    }

    /**
     * Remove jugadores
     *
     * @param \AppBundle\Model\Jugadores $jugadores
     */
    public function removeAudit(Jugadores $jugadores)
    {
        $this->jugadores->removeElement($jugadores);
    }
}




<?php
/**
 * Created by PhpStorm.
 * User: gustavo
 * Date: 21/05/17
 * Time: 11:09
 */

namespace AppBundle\Controller;

use AppBundle\Forms\ClubType;
use AppBundle\Forms\JugadoresType;
use AppBundle\Model\Club;
use AppBundle\Model\Jugadores;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class ClubController extends Controller
{
    /**
     * @param Request $request
     * @Route("/create_club", name="create_club")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function CreateClub(Request $request)
    {
        $session = new Session();
        $club = new Club();
        $form = $this->createForm(ClubType::class, $club);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->persist($club);
                $em->flush();
                $url = $this->generateUrl('create_club');
                $session->getFlashBag()->add('success', 'Profile for ' . $club->getNombre() . ' has been created created');

                return $this->redirect($url);

            } catch (\Doctrine\DBAL\DBALException $e) {
                $session->getFlashBag()->add('error', $e->getMessage());
            }
        }

        return $this->render('club_create.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @param Request $request
     * @param null $id
     *
     * @Route("/edit_club/{id}", name="edit_club")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function EditClub(Request $request, $id = null)
    {
        $session = new Session();
        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository('AppBundle:Club')->find($id);

        $form = $this->createForm(ClubType::class, $club);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            try {
                $em->flush();
                $session->getFlashBag()->add('success', 'Profile for ' . $club->getNombre() . ' has been edited.');

                return $this->render('club_edit.html.twig', ['form' => $form->createView()]);

            } catch (\Doctrine\DBAL\DBALException $e) {
                $session->getFlashBag()->add('error', $e->getMessage());
            }

        }

        return $this->render('club_edit.html.twig', ['form' => $form->createView()]);

    }

    /**
     *
     * @Route("/list_club", name="list_club")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ListClub()
    {
        $em = $this->getDoctrine()->getManager();
        $clubs =  $em->getRepository('AppBundle:Club')->findAll();



        return $this->render('club_list.html.twig', ['clubs' => $clubs]);


    }



}
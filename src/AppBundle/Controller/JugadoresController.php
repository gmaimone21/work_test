<?php
/**
 * Created by PhpStorm.
 * User: gustavo
 * Date: 21/05/17
 * Time: 11:09
 */

namespace AppBundle\Controller;

use AppBundle\Forms\JugadoresType;
use AppBundle\Model\Jugadores;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;



class JugadoresController extends Controller
{
    /**
     * @param Request $request
     * @Route("/create_jugadores", name="create_jugadores")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function CreateJugadores(Request $request)
    {
        $session = new Session();
        $jugadores = new Jugadores();
        $form = $this->createForm(JugadoresType::class, $jugadores);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em->persist($jugadores);
                $em->flush();
                $url = $this->generateUrl('create_jugadores');
                $session->getFlashBag()->add('success', 'Profile for ' . $jugadores->getNombre() . ' has been created created');

                return $this->redirect($url);

            } catch (\Doctrine\DBAL\DBALException $e) {
                $session->getFlashBag()->add('error', $e->getMessage());
            }
        }

        return $this->render('jugador_create.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @param Request $request
     * @param null $id
     *
     * @Route("/edit_jugadores/{id}", name="edit_jugadores")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function EditJugadores(Request $request, $id = null)
    {
        $session = new Session();
        $em = $this->getDoctrine()->getManager();
        $jugador = $em->getRepository('AppBundle:Jugadores')->find($id);

        $form = $this->createForm(JugadoresType::class, $jugador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            try {
                $em->flush();
                $session->getFlashBag()->add('success', 'Profile for ' . $jugador->getNombre() . ' has been edited.');

                return $this->render('jugador_edit.html.twig', ['form' => $form->createView()]);

            } catch (\Doctrine\DBAL\DBALException $e) {
                $session->getFlashBag()->add('error', $e->getMessage());
            }

        }

        return $this->render('jugador_edit.html.twig', ['form' => $form->createView()]);

    }

}
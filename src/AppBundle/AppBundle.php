<?php

namespace AppBundle;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    public function build(ContainerBuilder $containerBuilder)
    {
        parent::build($containerBuilder);

        $modelDir = dirname(__DIR__).'/AppBundle/Doctrine/ORM/mapping';
        $mappings = [$modelDir => 'AppBundle\Model'];

        $ormCompilerClass = 'Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass';
        if (class_exists($ormCompilerClass)) {
            $containerBuilder->addCompilerPass(DoctrineOrmMappingsPass::createYamlMappingDriver(
                $mappings,
                [],
                false,
                ['AppBundle' => 'AppBundle\Model']
            ));
        }
    }

}

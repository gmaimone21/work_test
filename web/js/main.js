/**
 * Created by gustavo on 21/05/17.
 */

$( document ).ready(function() {
    $("#club_telefono").mask("(99) 999-99-99-99");

    $("#club_telefono").on("blur", function() {
        var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

        if( last.length == 3 ) {
            var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
            var lastfour = move + last;

            var first = $(this).val().substr( 0, 9 );

            $(this).val( first + '-' + lastfour );
        }
    });

});
